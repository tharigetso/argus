import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class AdminPage extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JButton btnNewButton;
	private JButton btnPasswordChange;
	private JButton btnAddPhone;
	private JComboBox nameField;
	private JTextField brandField;
	private JTextField ramField;
	private JTextField cameraField;
	private String filename;
	private JTextField priceField;
	private JComboBox availabilityField;
	private JTextField warrentyField;
	private String phoneName;
	private JMenuBar menubar;
    private JMenu home;
    private JMenu phoneUpload;
    private JMenu phoneUpdate;
   
    private JMenu login;
    private JMenu signup;
	JButton btnDelete;
	JButton btnUpdate;
	JButton attachButton;
	JLabel imageLabel;

	private JButton btnLogout;
	private JButton btnSearch;
	private JMenuBar menuBar;
	private JMenuItem mntmNewMenuItem;
	private JSeparator separator_1;
	private JMenu mnNewMenu_2;
	private JMenuItem mntmNewMenuItem_1;
	private JMenuItem mntmNewMenuItem_2;
	private JMenuItem mntmNewMenuItem_3;
	private JSeparator separator_2;


	/**
	 * Create the application.
	 */
	public AdminPage() {
		initialize();
		
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("img\\STDM.jpg"));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(450, 190, 1014, 597);
		setResizable(true);
		Dimension size = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation(size.width / 2 - getWidth() / 2, size.height / 2 - getHeight() / 2);
		
		menuBar = new JMenuBar();
		menuBar.setForeground(new Color(255, 255, 255));
		menuBar.setBackground(new Color(0, 153, 255));
		setJMenuBar(menuBar);
		
		JMenu mnNewMenu = new JMenu("Customer");
		mnNewMenu.addActionListener(new ActionListener() {
			 
			public void actionPerformed(ActionEvent e) {
				
			}
		});
		mnNewMenu.setFont(new Font("Dialog", Font.PLAIN, 16));
		mnNewMenu.setBackground(new Color(0, 153, 255));
		menuBar.add(mnNewMenu);
		
		mntmNewMenuItem = new JMenuItem("Marketplace");
		mntmNewMenuItem.setFont(new Font("Segoe UI", Font.ITALIC, 16));
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CustomHome home = new CustomHome();
				home.setVisible(true);
				dispose();
			}
		});
		mnNewMenu.add(mntmNewMenuItem);
		
		JSeparator separator = new JSeparator();
		mnNewMenu.add(separator);
		
		JMenuItem mntmNewMenuItem_4 = new JMenuItem("Exit");
		mntmNewMenuItem_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(JFrame.EXIT_ON_CLOSE);
			}
		});
		
		mntmNewMenuItem_4.setFont(new Font("Dialog", Font.ITALIC, 16));
		mnNewMenu.add(mntmNewMenuItem_4);
		
		JMenu mnNewMenu_1 = new JMenu("Admin ");
		mnNewMenu_1.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				PhoneUpload phoneupload = new PhoneUpload();
				phoneupload.setVisible(true);
			}
		});
		mnNewMenu_1.setFont(new Font("Dialog", Font.PLAIN, 16));
		menuBar.add(mnNewMenu_1);
		
		JMenuItem mntmNewMenuItem_6 = new JMenuItem("Login");
		mntmNewMenuItem_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AdminLogin login = new AdminLogin();
				login.setVisible(true);
				dispose();
			}
		});
		mntmNewMenuItem_6.setFont(new Font("Dialog", Font.ITALIC, 16));
		mnNewMenu_1.add(mntmNewMenuItem_6);
		
		JMenuItem mntmNewMenuItem_7 = new JMenuItem("Admin Panel");
		mntmNewMenuItem_7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AdminPage page = new AdminPage();
				page.setVisible(true);
				dispose();
			}
		});
		
		separator_1 = new JSeparator();
		mnNewMenu_1.add(separator_1);
		mntmNewMenuItem_7.setFont(new Font("Dialog", Font.ITALIC, 16));
		mnNewMenu_1.add(mntmNewMenuItem_7);
		
		mnNewMenu_2 = new JMenu("Phones");
		mnNewMenu_2.setFont(new Font("Dialog", Font.ITALIC, 16));
		mnNewMenu_1.add(mnNewMenu_2);
		
		mntmNewMenuItem_1 = new JMenuItem("Add");
		mntmNewMenuItem_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PhoneUpload phoneupload = new PhoneUpload();
				phoneupload.setVisible(true);
				dispose();
			}
		});
		mntmNewMenuItem_1.setFont(new Font("Dialog", Font.ITALIC, 16));
		mnNewMenu_2.add(mntmNewMenuItem_1);
		
		mntmNewMenuItem_2 = new JMenuItem("Update ");
		mntmNewMenuItem_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			}
		});
		mntmNewMenuItem_2.setFont(new Font("Dialog", Font.ITALIC, 16));
		mnNewMenu_2.add(mntmNewMenuItem_2);
		
		separator_2 = new JSeparator();
		mnNewMenu_1.add(separator_2);
		
		mntmNewMenuItem_3 = new JMenuItem("Logout");
		mntmNewMenuItem_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AdminLogin login = new AdminLogin();
				login.setVisible(true);
				dispose();
			}
		});
		mntmNewMenuItem_3.setFont(new Font("Dialog", Font.ITALIC, 16));
		mnNewMenu_1.add(mntmNewMenuItem_3);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		getContentPane().setBackground(new Color (28,28,30));
		contentPane.setLayout(null);

		
		JLabel lblNewUserRegister = new JLabel("My Phones");
		lblNewUserRegister.setForeground(new Color(0, 153, 255));
		lblNewUserRegister.setFont(new Font("Times New Roman", Font.PLAIN, 42));
		lblNewUserRegister.setBounds(200, 52, 325, 50);
		contentPane.add(lblNewUserRegister);

		btnAddPhone = new JButton("Add phone");
		btnAddPhone.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PhoneUpload phoneUpload = new PhoneUpload();
				phoneUpload.setVisible(true);
				dispose();
			}

		});
		btnAddPhone.setFont(new Font("Dialog", Font.PLAIN, 18));
		btnAddPhone.setBackground(new Color(10,132,255));
	    btnAddPhone.setForeground(Color.BLACK);
		btnAddPhone.setBounds(721, 52, 130, 24);
		contentPane.add(btnAddPhone);


		btnPasswordChange = new JButton("My Admin");
		btnPasswordChange.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AdminUpdate phoneUpload = new AdminUpdate();
				phoneUpload.setVisible(true);
				dispose();
			}

		});
		btnPasswordChange.setFont(new Font("Dialog", Font.PLAIN, 18));
		btnPasswordChange.setBounds(858, 52, 130, 24);
		btnPasswordChange.setBackground(new Color(10,132,255));
		btnPasswordChange.setForeground(Color.BLACK);
		contentPane.add(btnPasswordChange);


		nameField = new JComboBox();
		nameField.setFont(new Font("Dialog", Font.BOLD, 20));
		nameField.setBounds(214, 129, 228, 22);
		nameField.setBackground(new Color (174,174,178));

		
		try {
			DBConfig connection = new DBConfig();
			String query = "SELECT pname FROM phone";

			Statement sta = connection.getConn().createStatement();
			ResultSet rs = sta.executeQuery(query);
			
			while (rs.next()) {
				nameField.addItem(rs.getString("pname"));
			
			}
			connection.getConn().close();
		} catch (Exception exception) {
					exception.printStackTrace();
		  }
		
		nameField.setFont(new Font("Tahoma", Font.PLAIN, 13));
		contentPane.add(nameField);

		btnSearch = new JButton("Search");
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String searchItem = (String) nameField.getItemAt(nameField.getSelectedIndex());

				try {
					DBConfig connection = new DBConfig();
					String query = "select * from phone where pname = '" + searchItem + "'";

					Statement sta = connection.getConn().createStatement();
					ResultSet rs = sta.executeQuery(query);

					if (!(rs.next())) {
						JOptionPane.showMessageDialog(btnNewButton, "No results found");
					} else {
						availabilityField.setSelectedItem(rs.getString("availability"));
						brandField.setText(rs.getString("brand"));
						ramField.setText(rs.getString("ram"));
						cameraField.setText(rs.getString("camera"));
						warrentyField.setText(rs.getString("warrenty"));
						priceField.setText(rs.getString("price"));
						phoneName = rs.getString("pname");

						byte[] imageData = rs.getBytes("image");
						ImageIcon icon = new ImageIcon(imageData);
						Image image = icon.getImage().getScaledInstance(imageLabel.getWidth(), imageLabel.getHeight(),
								Image.SCALE_SMOOTH);
						ImageIcon scaledIcon = new ImageIcon(image);
						imageLabel.setIcon(scaledIcon);

					}
					connection.getConn().close();
				} catch (Exception exception) {
					exception.printStackTrace();
				}
			}

		});
		btnSearch.setFont(new Font("Dialog", Font.BOLD, 16));
		btnSearch.setBounds(452, 127, 105, 24);
		btnSearch.setBackground(new Color(10,132,255));
	    btnSearch.setForeground(Color.BLACK);
		contentPane.add(btnSearch);

		JLabel availability = new JLabel("Availability");
		availability.setFont(new Font("Helvetica Neue", Font.PLAIN, 20));
		availability.setBounds(58, 200, 99, 24);
		availability.setFont(new Font("Helvetica Neue", Font.PLAIN, 20));
		availability.setForeground(new Color(10,132,255));
		contentPane.add(availability);

		JLabel ram = new JLabel("Brand");
		ram.setFont(new Font("Helvetica Neue", Font.PLAIN, 20));
		ram.setBounds(58, 250, 110, 24);
		ram.setFont(new Font("Helvetica Neue", Font.PLAIN, 20));
		ram.setForeground(new Color(10,132,255));
		contentPane.add(ram);

		JLabel sim = new JLabel("RAM");
		sim.setFont(new Font("Helvetica Neue", Font.PLAIN, 20));
		sim.setBounds(58, 300, 124, 24);
		sim.setFont(new Font("Helvetica Neue", Font.PLAIN, 20));
		sim.setForeground(new Color(10,132,255));
		contentPane.add(sim);

		availabilityField = new JComboBox();
		availabilityField.setFont(new Font("Dialog", Font.BOLD, 16));
		availabilityField.setBounds(214, 200, 228, 24);
		availabilityField.setBackground(new Color (174,174,178));
		availabilityField.addItem("no");
		availabilityField.addItem("yes");
		contentPane.add(availabilityField);

		brandField = new JTextField();
		brandField.setFont(new Font("Tahoma", Font.PLAIN, 15));
		brandField.setBounds(214, 250, 228, 24);
		brandField.setBackground(new Color (174,174,178));
		contentPane.add(brandField);
		brandField.setColumns(10);

		ramField = new JTextField();

		ramField.setFont(new Font("Tahoma", Font.PLAIN, 15));
		ramField.setBounds(214, 300, 228, 24);
		ramField.setBackground(new Color (174,174,178));
		contentPane.add(ramField);
		ramField.setColumns(10);

		cameraField = new JTextField();
		cameraField.setFont(new Font("Tahoma", Font.PLAIN, 15));
		cameraField.setBounds(214, 350, 228, 24);
		cameraField.setBackground(new Color (174,174,178));
		contentPane.add(cameraField);
		cameraField.setColumns(10);

		JLabel camera = new JLabel("Camera");
		camera.setFont(new Font("Tahoma", Font.PLAIN, 15));
		camera.setBounds(58, 350, 99, 24);
		camera.setFont(new Font("Helvetica Neue", Font.PLAIN, 20));
		camera.setForeground(new Color(10,132,255));
		contentPane.add(camera);

		JLabel warrenty = new JLabel("Warrenty");
		warrenty.setFont(new Font("Tahoma", Font.PLAIN, 15));
		warrenty.setBounds(58, 400, 99, 24);
		warrenty.setFont(new Font("Helvetica Neue", Font.PLAIN, 20));
		warrenty.setForeground(new Color(10,132,255));
		contentPane.add(warrenty);

		warrentyField = new JTextField();
		warrentyField.setFont(new Font("Tahoma", Font.PLAIN, 15));
		warrentyField.setBounds(214, 400, 228, 24);
		warrentyField.setBackground(new Color (174,174,178));
		contentPane.add(warrentyField);

		JLabel price = new JLabel("Price");
		price.setFont(new Font("Tahoma", Font.PLAIN, 15));
		price.setBounds(500, 400, 99, 24);
		price.setFont(new Font("Helvetica Neue", Font.PLAIN, 20));
		price.setForeground(new Color(10,132,255));
		contentPane.add(price);

		priceField = new JTextField();
		priceField.setFont(new Font("Tahoma", Font.PLAIN, 15));
		priceField.setBounds(570, 400, 228, 24);
		priceField.setBackground(new Color (174,174,178));
		contentPane.add(priceField);
		priceField.setColumns(10);

		imageLabel = new JLabel();
		imageLabel.setBounds(555, 200, 250, 180);
		contentPane.add(imageLabel);

		JButton attachButton = new JButton("Image");
		attachButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser();
				chooser.showOpenDialog(null);
				File f = chooser.getSelectedFile();
				filename = f.getAbsolutePath();

				Image getAbsolutePath = null;
				ImageIcon icon = new ImageIcon(filename);
				Image image = icon.getImage().getScaledInstance(imageLabel.getWidth(), imageLabel.getHeight(),
						Image.SCALE_SMOOTH);
				ImageIcon scaledIcon = new ImageIcon(image);
				imageLabel.setIcon(scaledIcon);
			}

		});
		attachButton.setFont(new Font("Dialog", Font.BOLD, 16));
		attachButton.setBounds(830, 400, 110, 24);
		attachButton.setBackground(new Color(10,132,255));
	    attachButton.setForeground(Color.BLACK);
		contentPane.add(attachButton);

		btnUpdate = new JButton("Update");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String brand = brandField.getText();
				String ram = ramField.getText();
				String camera = cameraField.getText();
				String warrenty = warrentyField.getText();
				String price = priceField.getText();
				String availability = availabilityField.getSelectedItem().toString();
				String pName = phoneName;

				try {
					DBConfig connection = new DBConfig();
					PreparedStatement ps;
					if (filename == null) {
						ps = connection.getConn().prepareStatement(
								"update phone set brand=? , ram=? , camera=? , warrenty=? , price=? , availability=? where pname=?");
						ps.setString(1, brand);
						ps.setString(2, ram);
						ps.setString(3, camera);
						ps.setString(4, warrenty);
						ps.setString(5, price);
						ps.setString(6, availability);
						ps.setString(7, pName);

					} else {
						InputStream is = new FileInputStream(new File(filename));
						ps = connection.getConn().prepareStatement(
								"update phone set brand=? , ram=? , camera=? , warrenty=? , price=? , availability=? , image=? where pname=?");

						ps.setString(1, brand);
						ps.setString(2, ram);
						ps.setString(3, camera);
						ps.setString(4, warrenty);
						ps.setString(5, price);
						ps.setString(6, availability);
						ps.setBlob(7, is);
						ps.setString(8, pName);

					}

					int x = ps.executeUpdate();
					if (x == 0) {
						JOptionPane.showMessageDialog(btnNewButton, "Something wrong could't update");
					} else {
						JOptionPane.showMessageDialog(btnNewButton, pName + " is sucessfully updated");
					}
					connection.getConn().close();

				} catch (SQLException | FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			}

		});
		btnUpdate.setFont(new Font("Dialog", Font.BOLD, 16));
		btnUpdate.setBounds(214, 450, 110, 24);
		btnUpdate.setBackground(new Color(10,132,255));
	    btnUpdate.setForeground(Color.BLACK);
		contentPane.add(btnUpdate);

		btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String pName = phoneName;

				try {
					DBConfig connection = new DBConfig();

					PreparedStatement ps = connection.getConn().prepareStatement("delete from phone where pname=?");

					ps.setString(1, pName);

					int x = ps.executeUpdate();
					if (x == 0) {
						JOptionPane.showMessageDialog(btnNewButton, "Something wrong could't delete");
					} else {
						JOptionPane.showMessageDialog(btnNewButton, pName + " is sucessfully deleted");
					}
					connection.getConn().close();

				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			}

		});
		btnDelete.setFont(new Font("Dialog", Font.BOLD, 16));
		btnDelete.setBounds(340, 450, 110, 24);
		btnDelete.setBackground(new Color(10,132,255));
	    btnDelete.setForeground(Color.BLACK);
		contentPane.add(btnDelete);
	}
}
