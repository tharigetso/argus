import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class PhoneUpload extends JFrame{

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField nameField;
	private JTextField brandField;
	private JTextField ramField;
	private JTextField cameraField;
	private String filename;
	private JTextField priceField;
	private JTextField warrentyField;
	private JButton btnNewButton;
	private JMenuBar menuBar;
	private JMenu mnNewMenu;
	private JMenu mnNewMenu_1;
	private JMenuItem mntmNewMenuItem;
	private JSeparator separator;
	private JMenuItem mntmNewMenuItem_1;
	private JMenuItem mntmNewMenuItem_2;
	private JMenuItem mntmNewMenuItem_3;
	private JSeparator separator_1;
	private JMenu mnNewMenu_2;
	private JMenuItem mntmNewMenuItem_4;
	private JMenuItem mntmNewMenuItem_5;
	private JSeparator separator_2;
	private JMenuItem mntmNewMenuItem_6;

	/**
	 * Create the application.
	 */
	public PhoneUpload() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("img\\STDM.jpg"));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(450, 190, 1014, 597);
		Dimension size = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation(size.width / 2 - getWidth() / 2, size.height / 2 - getHeight() / 2);
		setResizable(false);
		
		menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		menuBar.setBackground(new Color(0, 153, 255));
		
		mnNewMenu = new JMenu("Home");
		mnNewMenu.setFont(new Font("Dialog", Font.PLAIN, 16));
		
		menuBar.add(mnNewMenu);
		
		mntmNewMenuItem = new JMenuItem("Marketplace");
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CustomHome home = new CustomHome();
				home.setVisible(true);
				dispose();
			}
			
		});
		mntmNewMenuItem.setFont(new Font("Dialog", Font.ITALIC, 16));
		mnNewMenu.add(mntmNewMenuItem);
		
		separator = new JSeparator();
		mnNewMenu.add(separator);
		
		mntmNewMenuItem_1 = new JMenuItem("Exit");
		mntmNewMenuItem_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(JFrame.EXIT_ON_CLOSE);
			}
		});
		mntmNewMenuItem_1.setFont(new Font("Dialog", Font.ITALIC, 16));
		mnNewMenu.add(mntmNewMenuItem_1);
		
		mnNewMenu_1 = new JMenu("Admin");
		mnNewMenu_1.setFont(new Font("Dialog", Font.PLAIN, 16));
		menuBar.add(mnNewMenu_1);
		
		mntmNewMenuItem_2 = new JMenuItem("Login");
		mntmNewMenuItem_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AdminLogin login = new AdminLogin();
				login.setVisible(true);
				dispose();
			}
		});
		mntmNewMenuItem_2.setFont(new Font("Dialog", Font.ITALIC, 16));
		mnNewMenu_1.add(mntmNewMenuItem_2);
		
		separator_1 = new JSeparator();
		mnNewMenu_1.add(separator_1);
		
		mntmNewMenuItem_3 = new JMenuItem("Admin Panel");
		mntmNewMenuItem_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AdminPage page = new AdminPage();
				page.setVisible(true);
				dispose();
			}
		});
		mntmNewMenuItem_3.setFont(new Font("Dialog", Font.ITALIC, 16));
		mnNewMenu_1.add(mntmNewMenuItem_3);
		
		mnNewMenu_2 = new JMenu("Phone");
		mnNewMenu_2.setFont(new Font("Dialog", Font.ITALIC, 16));
		mnNewMenu_1.add(mnNewMenu_2);
		
		mntmNewMenuItem_4 = new JMenuItem("Add ");
		mntmNewMenuItem_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PhoneUpload phoneupload = new PhoneUpload();
				phoneupload.setVisible(true);
				dispose();
			}
		});
		mntmNewMenuItem_4.setFont(new Font("Dialog", Font.ITALIC, 16));
		mnNewMenu_2.add(mntmNewMenuItem_4);
		
		mntmNewMenuItem_5 = new JMenuItem("Update ");
		mntmNewMenuItem_5.setFont(new Font("Dialog", Font.ITALIC, 16));
		mnNewMenu_2.add(mntmNewMenuItem_5);
		mntmNewMenuItem_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AdminPage phoneupload = new AdminPage();
				phoneupload.setVisible(true);
				dispose();
			}
		});
		separator_2 = new JSeparator();
		mnNewMenu_1.add(separator_2);
		
		mntmNewMenuItem_6 = new JMenuItem("Logout");
		mntmNewMenuItem_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AdminLogin login = new AdminLogin();
				login.setVisible(true);
				dispose();
			}
		});
		mntmNewMenuItem_6.setFont(new Font("Dialog", Font.ITALIC, 16));
		mnNewMenu_1.add(mntmNewMenuItem_6);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		getContentPane().setBackground(new Color (28,28,30));
		contentPane.setLayout(null);

		JLabel lblNewUserRegister = new JLabel("Phone Upload");
		lblNewUserRegister.setFont(new Font("Helvetica Neue", Font.PLAIN, 42));
		lblNewUserRegister.setBounds(362, 52, 325, 50);
		lblNewUserRegister.setForeground(Color.WHITE);
		contentPane.add(lblNewUserRegister);

		JLabel phoneName = new JLabel("Phone name");
		phoneName.setFont(new Font("Helvetica Neue", Font.PLAIN, 20));
		phoneName.setBounds(58, 152, 130, 23);
		phoneName.setForeground(new Color(10,132,255));
		contentPane.add(phoneName);

		JLabel ram = new JLabel("Brand");
		ram.setFont(new Font("Helvetica Neue", Font.PLAIN, 20));
		ram.setForeground(new Color(10,132,255));
		ram.setBounds(58, 200, 110, 24);
		contentPane.add(ram);

		JLabel sim = new JLabel("RAM");
		sim.setFont(new Font("Helvetica Neue", Font.PLAIN, 20));
		sim.setForeground(new Color(10,132,255));
		sim.setBounds(58, 248, 124, 24);
		contentPane.add(sim);

		nameField = new JTextField();
		nameField.setFont(new Font("Helvetica Neue", Font.PLAIN, 20));
		nameField.setBounds(214, 151, 228, 24);
		nameField.setBackground(new Color (174,174,178));
		contentPane.add(nameField);
		nameField.setColumns(10);

		brandField = new JTextField();
		brandField.setFont(new Font("Helvetica Neue", Font.PLAIN, 20));
		brandField.setBounds(214, 200, 228, 24);
		brandField.setBackground(new Color (174,174,178));
		contentPane.add(brandField);
		brandField.setColumns(10);

		ramField = new JTextField();

		ramField.setFont(new Font("Helvetica Neue", Font.PLAIN, 20));
		ramField.setBounds(214, 248, 228, 24);
		ramField.setBackground(new Color (174,174,178));
		contentPane.add(ramField);
		ramField.setColumns(10);

		cameraField = new JTextField();
		cameraField.setFont(new Font("Helvetica Neue", Font.PLAIN, 20));
		cameraField.setBounds(214, 300, 228, 24);
		cameraField.setBackground(new Color (174,174,178));
		contentPane.add(cameraField);
		cameraField.setColumns(10);

		JLabel camera = new JLabel("Camera");
		camera.setFont(new Font("Helvetica Neue", Font.PLAIN, 20));
		camera.setForeground(new Color(10,132,255));
		camera.setBounds(58, 300, 99, 24);
		contentPane.add(camera);

		JLabel warrenty = new JLabel("Warrenty");
		warrenty.setFont(new Font("Helvetica Neue", Font.PLAIN, 20));
		warrenty.setForeground(new Color(10,132,255));
		warrenty.setBounds(58, 350, 99, 24);
		contentPane.add(warrenty);

		warrentyField = new JTextField();
		warrentyField.setBackground(new Color(153, 153, 153));
		warrentyField.setFont(new Font("Helvetica Neue", Font.PLAIN, 20));
		warrentyField.setBounds(214, 350, 228, 24);
		contentPane.add(warrentyField);

		JLabel price = new JLabel("Price");
		price.setFont(new Font("Helvetica Neue", Font.PLAIN, 20));
		price.setForeground(new Color(10,132,255));
		price.setBounds(500, 350, 99, 24);
		contentPane.add(price);

		priceField = new JTextField();
		priceField.setFont(new Font("Helvetica Neue", Font.PLAIN, 20));
		priceField.setBounds(570, 350, 228, 24);
		priceField.setBackground(new Color (174,174,178));
		contentPane.add(priceField);
		priceField.setColumns(10);

		JLabel imageLabel = new JLabel();
		imageLabel.setBounds(555, 152, 250, 180);
		contentPane.add(imageLabel);

		JButton attachButton = new JButton("Image");
		attachButton.setBackground(new Color(0, 153, 255));
		attachButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser();
				chooser.showOpenDialog(null);
				File f = chooser.getSelectedFile();
				filename = f.getAbsolutePath();

				Image getAbsolutePath = null;
				ImageIcon icon = new ImageIcon(filename);
				Image image = icon.getImage().getScaledInstance(imageLabel.getWidth(), imageLabel.getHeight(),
						Image.SCALE_SMOOTH);
				ImageIcon scaledIcon = new ImageIcon(image);
				imageLabel.setIcon(scaledIcon);
			}

		});
		attachButton.setFont(new Font("Dialog", Font.BOLD, 16));
		attachButton.setBounds(830, 350, 110, 24);
		contentPane.add(attachButton);

		btnNewButton = new JButton("Add");
		btnNewButton.setBackground(new Color(0, 153, 255));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String name = nameField.getText();
				String brand = brandField.getText();
				String ram = ramField.getText();
				String camera = cameraField.getText();
				String warrenty = warrentyField.getText();
				String price = priceField.getText();
				String fileName = filename;

				try {
					if(name==null || brand==null || ram==null || camera==null || warrenty==null || price==null || fileName==null) {
						JOptionPane.showMessageDialog(btnNewButton, "fill all the fields");
					}else {
					DBConfig connection = new DBConfig();
					String query = "select * from phone where pname = '" + name + "'";
					Statement sta = connection.getConn().createStatement();
					ResultSet rs = sta.executeQuery(query);

					if (rs.next()) {
						JOptionPane.showMessageDialog(btnNewButton, "Phone has already saved");
					} else {
						
						InputStream is = new FileInputStream(new File(fileName));
						
						PreparedStatement ps = connection.getConn().prepareStatement("insert into phone(pname,brand,ram,camera,warrenty,price,image,availability) values (?,?,?,?,?,?,?,?)");
						ps.setString(1, name);
						ps.setString(2, brand);
						ps.setString(3, ram);
						ps.setString(4, camera);
						ps.setString(5, warrenty);
						ps.setString(6, price);
						ps.setBlob(7, is);
						ps.setString(8, "yes");
						
						int x = ps.executeUpdate();
						if (x == 0) {
							JOptionPane.showMessageDialog(btnNewButton, "Something wrong could't update");
						} else {
							JOptionPane.showMessageDialog(btnNewButton, name + " is sucessfully saved");
						}
						connection.getConn().close();

					}
				}
				} catch (SQLException | FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnNewButton.setFont(new Font("Dialog", Font.BOLD, 16));
		btnNewButton.setBounds(399, 447, 110, 23);
		contentPane.add(btnNewButton);

	}

}
