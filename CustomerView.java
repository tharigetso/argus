
public class CustomerView {
	private byte[] phoneImage;
	private String phonePrice;
	private String phoneName;	
	
	public byte[] getPhoneImage() {
		return phoneImage;
	}
	public void setPhoneImage(byte[] phoneImage) {
		this.phoneImage = phoneImage;
	}
	public String getPhonePrice() {
		return phonePrice;
	}
	public void setPhonePrice(String phonePrice) {
		this.phonePrice = phonePrice;
	}
	public String getPhoneName() {
		return phoneName;
	}
	public void setPhoneName(String phoneName) {
		this.phoneName = phoneName;
	}
	

}
