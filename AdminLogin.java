
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

/**
 * 
 * @author javaguides.net
 *
 */
public class AdminLogin extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField adminUsername;
	private JPasswordField adminPassword;
	private JButton btnNewButton;

	public AdminLogin() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("img\\STDM.jpg"));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(450, 190, 1014, 597);
		Dimension size = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation(size.width / 2 - getWidth() / 2, size.height / 2 - getHeight() / 2);
		setResizable(false);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		getContentPane().setBackground(new Color (28,28,30));
		contentPane.setLayout(null);

		JLabel adminLogin = new JLabel("Admin login");
		adminLogin.setFont(new Font("Helvetica Neue", Font.PLAIN, 42));
		adminLogin.setForeground(Color.WHITE);
		adminLogin.setBounds(342, 50, 325, 50);
		contentPane.add(adminLogin);

		JLabel lblName = new JLabel("Username");
		lblName.setFont(new Font("Helvetica Neue", Font.PLAIN, 20));
		lblName.setForeground(new Color(10,132,255));
		lblName.setBounds(173, 150, 99, 43);
		contentPane.add(lblName);

		JLabel lblNewLabel = new JLabel("Password");
		lblNewLabel.setFont(new Font("Helvetica Neue", Font.PLAIN, 20));
		lblNewLabel.setForeground(new Color(10,132,255));
		lblNewLabel.setBounds(174, 248, 110, 29);
		contentPane.add(lblNewLabel);

		adminUsername = new JTextField();
		adminUsername.setFont(new Font("Helvetica Neu", Font.PLAIN, 20));
		adminUsername.setBackground(new Color (174,174,178));
		adminUsername.setBounds(391, 158, 228, 30);
		contentPane.add(adminUsername);
		adminUsername.setColumns(10);

		adminPassword = new JPasswordField();
		adminPassword.setFont(new Font("Helvetica Neu", Font.PLAIN, 20));
		adminPassword.setBackground(new Color (174,174,178));
		adminPassword.setBounds(391, 243, 228, 30);
		contentPane.add(adminPassword);
		adminPassword.setColumns(10);

		btnNewButton = new JButton("Login");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String username = adminUsername.getText();
				String password = adminPassword.getText();

				String msg = "" + username;
				msg += " \n";

				try {
					DBConfig connection = new DBConfig();
					String query = "select * from admin where username = '" + username + "' and password = '" + password
							+ "'";

					Statement sta = connection.getConn().createStatement();
					ResultSet rs = sta.executeQuery(query);

					if (!(rs.next())) {
						JOptionPane.showMessageDialog(btnNewButton, "Invalid Credentials");
					} else {
						JOptionPane.showMessageDialog(btnNewButton, "Welcome, " + msg + "Login successful");
						AdminPage phoneUpload = new AdminPage();
						phoneUpload.setVisible(true);
						dispose();
					}
					connection.getConn().close();
				} catch (Exception exception) {
					exception.printStackTrace();
				}
			}
		});
		btnNewButton.setFont(new Font("Helvetica Neue", Font.PLAIN, 22));
		btnNewButton.setBackground(new Color(10,132,255));
		btnNewButton.setForeground(Color.BLACK);
		btnNewButton.setBounds(455, 365, 100, 32);
		contentPane.add(btnNewButton);
	}

}
