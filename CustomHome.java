import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.Color;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.ImageIcon;

public class CustomHome extends JFrame{

	private JFrame frame;
	private JLabel lblShopName;
	private JButton btnSearch;
	private JComboBox jComboBox;
	
	private JButton btnExit;
	private JButton btnAdmin;
	private JButton btnSubForm;
	private JButton btnPendingItem;
	private JPanel panel;
	

	/**
	 * Create the application.
	 */
	public CustomHome() {
		initialize();
	
	}


	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {

		frame = new JFrame();
		frame.setTitle("Detail Request Form");
		frame.getContentPane().setBackground(new Color(135, 206, 235));
		frame.setFont(new Font("Arial Narrow", Font.BOLD, 40));
		frame.setBounds(100, 100, 840, 775);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.getContentPane().setLayout(null);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 0, 840, 740);
		Dimension size = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation(size.width / 2 - getWidth() / 2, size.height / 2 - getHeight() / 2);
		setResizable(false);
		panel = new JPanel();
		panel.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(panel);
		getContentPane().setBackground(Color.WHITE);
		panel.setLayout(null);	
		
		
		JPanel panel1 = new JPanel();
		panel1.setLayout(null);
		panel1.setBackground(new Color(30, 144, 255));
		panel1.setBounds(0, 0, 840, 35);
		panel.add(panel1);
		
		btnExit = new JButton("EXIT");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(JFrame.EXIT_ON_CLOSE);
			}
		});

		btnExit.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnExit.setBackground(new Color(30, 144, 255));
		btnExit.setBounds(21, 0, 77, 35);
		panel.add(btnExit);
		
		btnAdmin = new JButton("Admin Login");
		btnAdmin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AdminLogin login = new AdminLogin();
				login.setVisible(true);
				dispose();
			}
		});
		
		btnAdmin.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnAdmin.setBackground(new Color(30, 144, 255));
		btnAdmin.setBounds(100, 0, 111, 35);
		panel.add(btnAdmin);
		
		btnPendingItem = new JButton("Request Pending Item");
		btnPendingItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PendingItem requestPending = new PendingItem();
				requestPending.setVisible(true);
				dispose();
			}
		});
		
		btnPendingItem.setForeground(new Color(0, 0, 0));
		btnPendingItem.setBackground(Color.LIGHT_GRAY);
		btnPendingItem.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnPendingItem.setBounds(641, 0, 189, 35);
		frame.getContentPane().add(btnPendingItem);
		panel.add(btnPendingItem);
		
		btnSubForm = new JButton("SUBSCRIBE");
		btnSubForm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SubForm subscriptionForm = new SubForm();
				subscriptionForm.setVisible(true);
				dispose();
			}
		});
		
		btnSubForm.setForeground(new Color(0, 0, 0));
		btnSubForm.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnSubForm.setBackground(Color.LIGHT_GRAY);
		btnSubForm.setBounds(526, 0, 111, 35);
		panel.add(btnSubForm);
		
		jComboBox = new JComboBox();
				try {
					DBConfig connection = new DBConfig();
					String query = "SELECT pname FROM phone";
	
					Statement sta = connection.getConn().createStatement();
					ResultSet rs = sta.executeQuery(query);
					
					while (rs.next()) {
						jComboBox.addItem(rs.getString("pname"));
					
					}
					connection.getConn().close();
				} catch (Exception exception) {
							exception.printStackTrace();
				  }
				
		jComboBox.setBackground(Color.WHITE);
		jComboBox.setFont(new Font("Tahoma", Font.PLAIN, 13));
		jComboBox.setBounds(600, 68, 131, 22);
		panel.add(jComboBox);

		btnSearch = new JButton("Search");
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String searchItem = (String) jComboBox.getItemAt(jComboBox.getSelectedIndex());
				PhoneDetail phoneDetail = new PhoneDetail(searchItem);
				phoneDetail.setVisible(true);
				dispose();
			}

		});
		
		btnSearch.setBounds(741, 68, 89, 24);
		panel.add(btnSearch);
		
		List<CustomerView> viewList = new ArrayList();
		
		try {
			DBConfig connection = new DBConfig();
			String query = "select * from phone order by datetime desc limit 6";

			Statement sta = connection.getConn().createStatement();
			ResultSet rs = sta.executeQuery(query);
			
			while (rs.next()) {
				CustomerView view = new CustomerView();
				view.setPhoneImage(rs.getBytes("image"));
				view.setPhoneName(rs.getString("pname"));
				view.setPhonePrice(rs.getString("price"));
				viewList.add(view);
			}
			connection.getConn().close();
		} catch (Exception exception) {
					exception.printStackTrace();
		  }

		ImageIcon imageIcon = new ImageIcon(viewList.get(0).getPhoneImage());
		JLabel phoneImage_0 = new JLabel();
		phoneImage_0.setIcon(new ImageIcon(imageIcon.getImage()));
		phoneImage_0.setBounds(42, 134, 131, 194);
		frame.getContentPane().add(phoneImage_0);
		panel.add(phoneImage_0);
		
		ImageIcon imageIcon1 = new ImageIcon(viewList.get(1).getPhoneImage());
		JLabel phoneImage_1 = new JLabel();
		phoneImage_1.setIcon(new ImageIcon(imageIcon1.getImage()));
		phoneImage_1.setBounds(301, 134, 131, 194);
		frame.getContentPane().add(phoneImage_1);
		panel.add(phoneImage_1);
		
		ImageIcon imageIcon2 = new ImageIcon(viewList.get(2).getPhoneImage());
		JLabel phoneImage_2 = new JLabel();
		phoneImage_2.setIcon(new ImageIcon(imageIcon2.getImage()));
		phoneImage_2.setBounds(559, 134, 131, 194);
		frame.getContentPane().add(phoneImage_2);
		panel.add(phoneImage_2);
		
		ImageIcon imageIcon3 = new ImageIcon(viewList.get(3).getPhoneImage());
		JLabel phoneImage_3 = new JLabel();
		phoneImage_3.setIcon(new ImageIcon(imageIcon3.getImage()));
		phoneImage_3.setBounds(42, 405, 131, 194);
		frame.getContentPane().add(phoneImage_3);
		panel.add(phoneImage_3);
		
		ImageIcon imageIcon4 = new ImageIcon(viewList.get(4).getPhoneImage());
		JLabel phoneImage_4 = new JLabel();
		phoneImage_4.setIcon(new ImageIcon(imageIcon4.getImage()));
		phoneImage_4.setBounds(311, 405, 131, 194);
		frame.getContentPane().add(phoneImage_4);
		panel.add(phoneImage_4);
		
		ImageIcon imageIcon5 = new ImageIcon(viewList.get(5).getPhoneImage());
		JLabel phoneImage_5 = new JLabel("New label");
		phoneImage_5.setIcon(new ImageIcon(imageIcon5.getImage()));
		phoneImage_5.setBounds(559, 405, 131, 194);
		frame.getContentPane().add(phoneImage_5);
		panel.add(phoneImage_5);
		
		JLabel lblPhoneName_0 = new JLabel(viewList.get(0).getPhoneName());
		lblPhoneName_0.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblPhoneName_0.setBounds(42, 326, 142, 25);
		frame.getContentPane().add(lblPhoneName_0);
		panel.add(lblPhoneName_0);
		
		JLabel lblPhoneName_1 = new JLabel(viewList.get(1).getPhoneName());
		lblPhoneName_1.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblPhoneName_1.setBounds(311, 326, 125, 25);
		frame.getContentPane().add(lblPhoneName_1);
		panel.add(lblPhoneName_1);
		
		JLabel lblPhoneName_2 = new JLabel(viewList.get(2).getPhoneName());
		lblPhoneName_2.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblPhoneName_2.setBounds(565, 326, 75, 25);
		frame.getContentPane().add(lblPhoneName_2);
		panel.add(lblPhoneName_2);
		
		JLabel lblPhoneName_3 = new JLabel(viewList.get(3).getPhoneName());
		lblPhoneName_3.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblPhoneName_3.setBounds(42, 594, 142, 25);
		frame.getContentPane().add(lblPhoneName_3);
		panel.add(lblPhoneName_3);
		
		JLabel lblPhoneName_4 = new JLabel(viewList.get(4).getPhoneName());
		lblPhoneName_4.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblPhoneName_4.setBounds(311, 594, 125, 25);
		frame.getContentPane().add(lblPhoneName_4);
		panel.add(lblPhoneName_4);
		
		JLabel lblPhoneName_5 = new JLabel(viewList.get(5).getPhoneName());
		lblPhoneName_5.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblPhoneName_5.setBounds(565, 594, 125, 25);
		frame.getContentPane().add(lblPhoneName_5);
		panel.add(lblPhoneName_5);
		
		JLabel lblPhonePrice_0 = new JLabel(viewList.get(0).getPhonePrice());
		lblPhonePrice_0.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblPhonePrice_0.setForeground(Color.RED);
		lblPhonePrice_0.setBounds(69, 349, 88, 22);
		frame.getContentPane().add(lblPhonePrice_0);
		panel.add(lblPhonePrice_0);
		
		JLabel lblPhonePrice_1 = new JLabel(viewList.get(1).getPhonePrice());
		lblPhonePrice_1.setForeground(Color.RED);
		lblPhonePrice_1.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblPhonePrice_1.setBounds(332, 349, 88, 22);
		frame.getContentPane().add(lblPhonePrice_1);
		panel.add(lblPhonePrice_1);
		
		JLabel lblPhonePrice_2 = new JLabel(viewList.get(2).getPhonePrice());
		lblPhonePrice_2.setForeground(Color.RED);
		lblPhonePrice_2.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblPhonePrice_2.setBounds(589, 349, 88, 22);
		frame.getContentPane().add(lblPhonePrice_2);
		panel.add(lblPhonePrice_2);
		
		JLabel lblPhonePrice_3 = new JLabel(viewList.get(3).getPhonePrice());
		lblPhonePrice_3.setForeground(Color.RED);
		lblPhonePrice_3.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblPhonePrice_3.setBounds(63, 615, 88, 22);
		frame.getContentPane().add(lblPhonePrice_3);
		panel.add(lblPhonePrice_3);
		
		JLabel lblPhonePrice_4 = new JLabel(viewList.get(4).getPhonePrice());
		lblPhonePrice_4.setForeground(Color.RED);
		lblPhonePrice_4.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblPhonePrice_4.setBounds(332, 615, 88, 22);
		frame.getContentPane().add(lblPhonePrice_4);
		panel.add(lblPhonePrice_4);
		
		JLabel lblPhonePrice_5 = new JLabel(viewList.get(5).getPhonePrice());
		lblPhonePrice_5.setForeground(Color.RED);
		lblPhonePrice_5.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblPhonePrice_5.setBounds(589, 615, 88, 22);
		frame.getContentPane().add(lblPhonePrice_5);
		panel.add(lblPhonePrice_5);
		
		lblShopName = new JLabel("ARGUS PHONE SHOP");
		lblShopName.setForeground(new Color(30, 144, 255));
		lblShopName.setBackground(Color.WHITE);
		lblShopName.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblShopName.setBounds(282, 36, 200, 37);
		panel.add(lblShopName);
		
		JPanel panel_1 = new JPanel();
		panel_1.setLayout(null);
		panel_1.setBackground(Color.LIGHT_GRAY);
		panel_1.setBounds(0, 663, 840, 25);
		panel.add(panel_1);
		
		JLabel lblNewLabel_4 = new JLabel("Latest Versions...");
		lblNewLabel_4.setForeground(Color.DARK_GRAY);
		lblNewLabel_4.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 13));
		lblNewLabel_4.setBounds(10, 90, 125, 22);
		frame.getContentPane().add(lblNewLabel_4);
		panel.add(lblNewLabel_4);
	}

}
