import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import javax.swing.JComboBox;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.swing.JButton;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.ImageIcon;

public class PhoneDetail extends JFrame implements ActionListener {

	private JPanel contentPane;
	private JTextField txtPname;
	private JTextField txtBrand;
	private JTextField txtRam;
	private JTextField txtCamera;
	private JTextField txtWarranty;
	private JTextField txtPrice;
	private JTextField txtAvailability;
	
	private JButton btnExit;
	private JButton btnCustomerHome;
	
	private String phoneDetail;
	ResultSet rs;
	JLabel lblImage;


	/**
	 * Create the frame.
	 */
	public PhoneDetail(String phoneDetail) {
		this.phoneDetail = phoneDetail;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 784, 591);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBackground(new Color(30, 144, 255));
		panel.setBounds(0, 0, 784, 35);
		contentPane.add(panel);
		
		btnExit = new JButton("EXIT");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(JFrame.EXIT_ON_CLOSE);
			}
		});

		btnExit.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnExit.setBackground(new Color(30, 144, 255));
		btnExit.setBounds(21, 0, 77, 35);
		panel.add(btnExit);
		
		btnCustomerHome = new JButton("CUSTOMER HOME");
		btnCustomerHome.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CustomHome customHome = new CustomHome();
				customHome.setVisible(true);
				dispose();
			}
		});
		
		btnCustomerHome.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnCustomerHome.setBackground(new Color(30, 144, 255));
		btnCustomerHome.setBounds(100, 0, 150, 35);
		panel.add(btnCustomerHome);
		
		JPanel panel_1 = new JPanel();
		panel_1.setLayout(null);
		panel_1.setBackground(Color.LIGHT_GRAY);
		panel_1.setBounds(0, 520, 784, 35);
		contentPane.add(panel_1);
		
		JLabel lblNewLabel_4 = new JLabel("PHONE DETAILS");
		lblNewLabel_4.setForeground(new Color(30, 144, 255));
		lblNewLabel_4.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblNewLabel_4.setBackground(Color.WHITE);
		lblNewLabel_4.setBounds(310, 45, 161, 37);
		contentPane.add(lblNewLabel_4);
		
		try {
			DBConfig connection = new DBConfig();
			String query = "SELECT * FROM phone WHERE pname = '" + phoneDetail + "'";

			Statement sta = connection.getConn().createStatement();
			rs = sta.executeQuery(query);
			rs.next();
					
			ImageIcon icon = new ImageIcon(rs.getBytes("image"));		
			JLabel lblImage = new JLabel();
			lblImage.setIcon(new ImageIcon(icon.getImage()));
			lblImage.setBounds(87, 126, 133, 194);
			contentPane.add(lblImage);
			
			JLabel lblNewLabel_1 = new JLabel("PHONE");
			lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblNewLabel_1.setBounds(360, 126, 67, 25);
			contentPane.add(lblNewLabel_1);
			
			JLabel lblNewLabel_1_1 = new JLabel("BRAND");
			lblNewLabel_1_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblNewLabel_1_1.setBounds(360, 162, 67, 25);
			contentPane.add(lblNewLabel_1_1);
			
			JLabel lblNewLabel_1_2 = new JLabel("RAM");
			lblNewLabel_1_2.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblNewLabel_1_2.setBounds(360, 198, 67, 25);
			contentPane.add(lblNewLabel_1_2);
			
			JLabel lblNewLabel_1_3 = new JLabel("CAMERA");
			lblNewLabel_1_3.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblNewLabel_1_3.setBounds(360, 234, 67, 25);
			contentPane.add(lblNewLabel_1_3);
			
			JLabel lblNewLabel_1_4 = new JLabel("WARRANTY");
			lblNewLabel_1_4.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblNewLabel_1_4.setBounds(360, 270, 83, 25);
			contentPane.add(lblNewLabel_1_4);
			
			JLabel lblNewLabel_1_5 = new JLabel("PRICE (Rs)");
			lblNewLabel_1_5.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblNewLabel_1_5.setBounds(360, 349, 67, 25);
			contentPane.add(lblNewLabel_1_5);
			
			txtPname = new JTextField();
			txtPname.setFont(new Font("Tahoma", Font.PLAIN, 13));
			txtPname.setText(rs.getString("pname"));
			txtPname.setColumns(10);
			txtPname.setBounds(468, 130, 139, 25);
			contentPane.add(txtPname);
			
			txtBrand = new JTextField();
			txtBrand.setFont(new Font("Tahoma", Font.PLAIN, 13));
			txtBrand.setText(rs.getString("brand"));
			txtBrand.setColumns(10);
			txtBrand.setBounds(468, 163, 139, 25);
			contentPane.add(txtBrand);
			
			txtRam = new JTextField();
			txtRam.setFont(new Font("Tahoma", Font.PLAIN, 13));
			txtRam.setText(rs.getString("ram"));
			txtRam.setColumns(10);
			txtRam.setBounds(468, 199, 139, 25);
			contentPane.add(txtRam);
			
			txtCamera = new JTextField();
			txtCamera.setFont(new Font("Tahoma", Font.PLAIN, 13));
			txtCamera.setText(rs.getString("camera"));
			txtCamera.setColumns(10);
			txtCamera.setBounds(468, 235, 139, 25);
			contentPane.add(txtCamera);
			
			txtWarranty = new JTextField();
			txtWarranty.setFont(new Font("Tahoma", Font.PLAIN, 13));
			txtWarranty.setText(rs.getString("warrenty"));
			txtWarranty.setColumns(10);
			txtWarranty.setBounds(468, 271, 139, 25);
			contentPane.add(txtWarranty);
			
			txtPrice = new JTextField();
			txtPrice.setFont(new Font("Tahoma", Font.BOLD, 13));
			txtPrice.setForeground(Color.RED);
			txtPrice.setText(rs.getString("price"));
			txtPrice.setColumns(10);
			txtPrice.setBounds(468, 353, 139, 25);
			contentPane.add(txtPrice);
			
			JLabel lblNewLabel_1_4_1 = new JLabel("AVAILABILITY");
			lblNewLabel_1_4_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblNewLabel_1_4_1.setBounds(360, 313, 83, 25);
			contentPane.add(lblNewLabel_1_4_1);
			
			txtAvailability = new JTextField();
			txtAvailability.setFont(new Font("Tahoma", Font.PLAIN, 13));
			txtAvailability.setText(rs.getString("availability"));
			txtAvailability.setColumns(10);
			txtAvailability.setBounds(468, 317, 139, 25);
			contentPane.add(txtAvailability);
		
		    connection.getConn().close();
		} catch (Exception exception) {
					exception.printStackTrace();
	  }
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}

}
