import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class AdminUpdate extends JFrame implements ActionListener{
	private static final long serialVersionUID = 1L;
    private JPanel contentPane;
    private JTextField adminCurrentUsername;
    private JPasswordField adminCurrentPassword;
    private JTextField adminNewUsername;
    private JPasswordField adminNewPassword;
    private JMenuBar menubar;
    private JMenu home;
    private JMenu phoneUpload;
    private JMenu phoneUpdate;
   
    private JMenu login;
    private JMenu signup;

    private JButton btnNewButton;
    private JMenuBar menuBar;
    private JMenu mnNewMenu;
    private JMenu mnNewMenu_1;
    private JMenuItem mntmNewMenuItem;
    private JSeparator separator;
    private JMenuItem mntmNewMenuItem_1;
    private JMenuItem mntmNewMenuItem_3;
    private JMenuItem mntmNewMenuItem_4;
    private JMenu mnNewMenu_3;
    private JMenuItem mntmNewMenuItem_2;
    private JMenuItem mntmNewMenuItem_7;
    private JSeparator separator_1;
    private JSeparator separator_2;
    private JMenuItem mntmNewMenuItem_5;
	

	/**
	 * Create the application.
	 */
	public AdminUpdate() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		 setIconImage(Toolkit.getDefaultToolkit().getImage("img\\STDM.jpg"));
	        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	        setBounds(450, 190, 1014, 597);
	        Dimension size = Toolkit.getDefaultToolkit().getScreenSize();
			setLocation(size.width/2-getWidth()/2, size.height/2-getHeight()/2);
	        setResizable(false);
	        
	        menuBar = new JMenuBar();
	        menuBar.setForeground(new Color(255, 255, 255));
	        menuBar.setBackground(new Color(0, 153, 255));
	        setJMenuBar(menuBar);
	        
	        mnNewMenu = new JMenu("Home");
	        mnNewMenu.setFont(new Font("Dialog", Font.PLAIN, 16));
	        menuBar.setBackground(new Color(0, 153, 255));
	        menuBar.add(mnNewMenu);
	        
	        mntmNewMenuItem = new JMenuItem("Marketplace");
	        mntmNewMenuItem.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent e) {
	        		CustomHome home = new CustomHome();
					home.setVisible(true);
					dispose();
	        	}
	        });
	        mntmNewMenuItem.setFont(new Font("Dialog", Font.ITALIC, 16));
	        mnNewMenu.add(mntmNewMenuItem);
	        
	        separator = new JSeparator();
	        mnNewMenu.add(separator);
	        
	        mntmNewMenuItem_1 = new JMenuItem("Exit");
	        mntmNewMenuItem_1.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent e) {
	        		System.exit(JFrame.EXIT_ON_CLOSE);
	        	}
	        });
	        mntmNewMenuItem_1.setFont(new Font("Dialog", Font.ITALIC, 16));
	        mnNewMenu.add(mntmNewMenuItem_1);
	        
	        mnNewMenu_1 = new JMenu("Admin");
	        mnNewMenu_1.setFont(new Font("Dialog", Font.PLAIN, 16));
	        menuBar.add(mnNewMenu_1);
	        
	        mntmNewMenuItem_3 = new JMenuItem("Login");
	        mntmNewMenuItem_3.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent e) {
	        		AdminLogin login = new AdminLogin();
					login.setVisible(true);
					dispose();
	        	}
	        });
	        mntmNewMenuItem_3.setFont(new Font("Dialog", Font.ITALIC, 16));
	        mnNewMenu_1.add(mntmNewMenuItem_3);
	        
	        separator_1 = new JSeparator();
	        mnNewMenu_1.add(separator_1);
	        
	        mntmNewMenuItem_4 = new JMenuItem("Admin Panel");
	        mntmNewMenuItem_4.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent e) {
	        		AdminPage page = new AdminPage();
					page.setVisible(true);
					dispose();
	        	}
	        });
	        mntmNewMenuItem_4.setFont(new Font("Dialog", Font.ITALIC, 16));
	        mnNewMenu_1.add(mntmNewMenuItem_4);
	        
	        mnNewMenu_3 = new JMenu("Phones");
	        mnNewMenu_3.setFont(new Font("Dialog", Font.ITALIC, 16));
	        mnNewMenu_1.add(mnNewMenu_3);
	        
	        mntmNewMenuItem_2 = new JMenuItem("Add");
	        mntmNewMenuItem_2.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent e) {
	        		PhoneUpload phoneupload = new PhoneUpload();
					phoneupload.setVisible(true);
					dispose();
	        	}
	        });
	        mntmNewMenuItem_2.setFont(new Font("Dialog", Font.ITALIC, 16));
	        mnNewMenu_3.add(mntmNewMenuItem_2);
	        
	        mntmNewMenuItem_7 = new JMenuItem("Update");
	        mntmNewMenuItem_7.setFont(new Font("Dialog", Font.ITALIC, 16));
	        mnNewMenu_3.add(mntmNewMenuItem_7);
	        mntmNewMenuItem_7.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					AdminPage adminpage = new AdminPage();
					adminpage.setVisible(true);
					dispose();
					
				}
			});
	        separator_2 = new JSeparator();
	        mnNewMenu_1.add(separator_2);
	        
	        mntmNewMenuItem_5 = new JMenuItem("Logout");
	        mntmNewMenuItem_5.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent e) {
	        		AdminLogin login = new AdminLogin();
	        		login.setVisible(true);
					dispose();
	        	}
	        });
	        mntmNewMenuItem_5.setFont(new Font("Dialog", Font.ITALIC, 16));
	        mnNewMenu_1.add(mntmNewMenuItem_5);
	        contentPane = new JPanel();
	        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
	        setContentPane(contentPane);
	        getContentPane().setBackground(new Color (28,28,30));
	        contentPane.setLayout(null);

	        
	        JLabel adminLogin = new JLabel("Update Admin Details");
	        adminLogin.setForeground(new Color(0, 153, 255));
	        adminLogin.setFont(new Font("Times New Roman", Font.PLAIN, 42));
	        adminLogin.setBounds(284, 52, 442, 50);
	        contentPane.add(adminLogin);


	        JLabel lblName = new JLabel("Current Username");
	        lblName.setForeground(new Color(0, 153, 255));
	        lblName.setFont(new Font("Dialog", Font.PLAIN, 20));
	        lblName.setBounds(58, 152, 200, 43);
	        contentPane.add(lblName);

	        JLabel lblNewLabel = new JLabel("Current Password");
	        lblNewLabel.setForeground(new Color(0, 153, 255));
	        lblNewLabel.setFont(new Font("Dialog", Font.PLAIN, 20));
	        lblNewLabel.setBounds(58, 243, 200, 29);
	        contentPane.add(lblNewLabel);

	        JLabel lblName1 = new JLabel("New Username");
	        lblName1.setForeground(new Color(0, 153, 255));
	        lblName1.setFont(new Font("Dialog", Font.PLAIN, 20));
	        lblName1.setBounds(542, 159, 200, 29);
	        contentPane.add(lblName1);

	        JLabel lblNewLabel1 = new JLabel("New Password");
	        lblNewLabel1.setForeground(new Color(0, 153, 255));
	        lblNewLabel1.setFont(new Font("Dialog", Font.PLAIN, 20));
	        lblNewLabel1.setBounds(542, 245, 200, 24);
	        contentPane.add(lblNewLabel1);

	        adminCurrentUsername = new JTextField();
	        adminCurrentUsername.setBackground(new Color(153, 153, 153));
	        adminCurrentUsername.setForeground(new Color(0, 0, 0));
	        adminCurrentUsername.setFont(new Font("Dialog", Font.PLAIN, 20));
	        adminCurrentUsername.setBounds(240, 154, 228, 30);
	        contentPane.add(adminCurrentUsername);
	        adminCurrentUsername.setColumns(10);

	        adminCurrentPassword = new JPasswordField();
	        adminCurrentPassword.setBackground(new Color(153, 153, 153));
	        adminCurrentPassword.setForeground(Color.BLACK);
	        adminCurrentPassword.setFont(new Font("Dialog", Font.PLAIN, 20));
	        adminCurrentPassword.setBounds(240, 239, 228, 30);
	        contentPane.add(adminCurrentPassword);
	        adminCurrentPassword.setColumns(10);
	        
	        
  
	        adminNewUsername = new JTextField();
	        adminNewUsername.setBackground(new Color(153, 153, 153));
	        adminNewUsername.setForeground(Color.BLACK);
	        adminNewUsername.setFont(new Font("Dialog", Font.PLAIN, 20));
	        adminNewUsername.setBounds(707, 153, 228, 30);
	        contentPane.add(adminNewUsername);
	        adminNewUsername.setColumns(10);

	        adminNewPassword = new JPasswordField();
	        adminNewPassword.setBackground(new Color(153, 153, 153));
	        adminNewPassword.setForeground(Color.BLACK);
	        adminNewPassword.setFont(new Font("Dialog", Font.PLAIN, 20));
	        adminNewPassword.setBounds(707, 236, 228, 30);
	        contentPane.add(adminNewPassword);
	        adminNewPassword.setColumns(10);

	        btnNewButton = new JButton("Update");
	        btnNewButton.setBackground(new Color(0, 153, 255));
	        btnNewButton.addActionListener(new ActionListener() {
	            public void actionPerformed(ActionEvent e) {
	                String current_username = adminCurrentUsername.getText();
	                String current_password = adminCurrentPassword.getText();
	                String new_username = adminNewUsername.getText();
	                String new_password = adminNewPassword.getText();
	                
	             

	                String msg = "" + new_username;
	                msg += " \n";
	                

	                try {
	                	DBConfig connection = new DBConfig();
	                    String query = "select * from admin where username = '"+current_username+"' and password = '"+current_password+"'";
	                 

	                    Statement sta = connection.getConn().createStatement();
	                    ResultSet rs = sta.executeQuery(query);
	                    
	                    if (!(rs.next())) {
	                        JOptionPane.showMessageDialog(btnNewButton, "Please enter correct current admin credentials");
	                    } else {
	                        
	                        	String query1 = "UPDATE admin set username='" + new_username + "', password='" + new_password + "'where username='" + current_username + "' and password='" + current_password + "'";
	                        	int x = sta.executeUpdate(query1);
	                        	if (x == 0) {
	                                JOptionPane.showMessageDialog(btnNewButton, "Something wrong could't update");
	                            } else {
	                        	JOptionPane.showMessageDialog(btnNewButton,"Welcome, " + msg + "update successful");
	                            }
	                    }
	                    connection.getConn().close();
	                } catch (Exception exception) {
	                    exception.printStackTrace();
	                }
	            }
	        });
	        btnNewButton.setFont(new Font("Dialog", Font.PLAIN, 20));
	        btnNewButton.setBounds(450, 350, 110, 32);
	        contentPane.add(btnNewButton);

	}
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource() == login) {
			this.dispose();
			new AdminLogin().show();
			}
		if(e.getSource() == signup) {
			this.dispose();
			//new Signup().show();
			}
		
	}

}
