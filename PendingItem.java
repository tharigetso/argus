// S14245 Pending Item Detail Request Form

//package forms;

import java.awt.EventQueue; 
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.NumberFormat;
import java.util.Hashtable;
 
import javax.swing.JTextField;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JFormattedTextField;
import javax.swing.text.NumberFormatter;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.SystemColor;
import java.awt.Toolkit;


public class PendingItem extends JFrame implements ActionListener {

	private JFrame frame;
	private JTextField name;
	private JTextField emailAddress;
	private JTextField label94;
	private JLabel heading;
	private JLabel nameLabel;
	private JLabel contactDetailsLabel;
	private JLabel mobNumLabel;
	private JLabel emailAddLabel;
	private JLabel prefLabel;
	private JLabel itemDetailsLabel;
	private JLabel brandLabel;
	private JLabel itemNameLabel;
	private JLabel storageLabel;
	private JLabel colon1;
	private JLabel colon2;
	private JLabel colon3;
	private JLabel colon4;
	private JLabel colon5;
	private JLabel colon6;
	private JLabel colon7;
	private JFormattedTextField mobileNum;
	private NumberFormat format;
	private NumberFormatter formatter;
	private JComboBox<Object> contactMode;
	private JComboBox<String> brandCBox;
	private JComboBox<String> itemNameCBox;
	private Hashtable<String, String[]> subItems = new Hashtable<String, String[]>();
	private JComboBox<String> storageCBox;
	private JButton btnRequest;
	private JButton btnClear;
	private JButton btnCancel;
	
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	

	/**
	 * Create the application.
	 */
	public PendingItem() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		frame = new JFrame();
		frame.setTitle("Detail Request Form");
		frame.getContentPane().setBackground(new Color(135, 206, 235));
		frame.setFont(new Font("Arial Narrow", Font.BOLD, 40));
		frame.setBounds(100, 100, 800, 775);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.getContentPane().setLayout(null);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 775);
		Dimension size = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation(size.width / 2 - getWidth() / 2, size.height / 2 - getHeight() / 2);
		setResizable(false);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		getContentPane().setBackground(new Color (135, 206, 235));
		contentPane.setLayout(null);

		/*
		 *  Labels
		 */
		
		heading = new JLabel("Detail Request for Pending Items");
		heading.setFont(new Font("Footlight MT Light", Font.BOLD, 35));
		heading.setHorizontalAlignment(SwingConstants.CENTER);
		heading.setBounds(100, 30, 600, 60);
		frame.getContentPane().add(heading);
		contentPane.add(heading);
		
		nameLabel = new JLabel("Name");
		nameLabel.setFont(new Font("Book Antiqua", Font.BOLD, 23));
		nameLabel.setBounds(50, 125, 100, 35);
		frame.getContentPane().add(nameLabel);
		contentPane.add(nameLabel);
		
		contactDetailsLabel = new JLabel("Contact Details");
		contactDetailsLabel.setFont(new Font("Book Antiqua", Font.BOLD, 23));
		contactDetailsLabel.setBounds(50, 180, 250, 35);
		frame.getContentPane().add(contactDetailsLabel);
		contentPane.add(contactDetailsLabel);
		
		mobNumLabel = new JLabel("Mobile Number");
		mobNumLabel.setFont(new Font("Book Antiqua", Font.BOLD, 20));
		mobNumLabel.setBounds(125, 225, 170, 35);
		frame.getContentPane().add(mobNumLabel);
		contentPane.add(mobNumLabel);
		
		emailAddLabel = new JLabel("Email Address");
		emailAddLabel.setFont(new Font("Book Antiqua", Font.BOLD, 20));
		emailAddLabel.setBounds(125, 275, 170, 35);
		frame.getContentPane().add(emailAddLabel);
		contentPane.add(emailAddLabel);
		
		prefLabel = new JLabel("Preferred mode of contact");
		prefLabel.setFont(new Font("Book Antiqua", Font.BOLD, 20));
		prefLabel.setBounds(50, 325, 250, 35);
		frame.getContentPane().add(prefLabel);
		contentPane.add(prefLabel);
		
		itemDetailsLabel = new JLabel("Item Details");
		itemDetailsLabel.setFont(new Font("Book Antiqua", Font.BOLD, 23));
		itemDetailsLabel.setBounds(50, 380, 250, 35);
		frame.getContentPane().add(itemDetailsLabel);
		contentPane.add(itemDetailsLabel);
		
		brandLabel = new JLabel("Brand");
		brandLabel.setFont(new Font("Book Antiqua", Font.BOLD, 20));
		brandLabel.setBounds(125, 425, 170, 35);
		frame.getContentPane().add(brandLabel);
		contentPane.add(brandLabel);
		
		itemNameLabel = new JLabel("Item Name");
		itemNameLabel.setFont(new Font("Book Antiqua", Font.BOLD, 20));
		itemNameLabel.setBounds(125, 475, 170, 35);
		frame.getContentPane().add(itemNameLabel);
		contentPane.add(itemNameLabel);
		
		storageLabel = new JLabel("RAM");
		storageLabel.setFont(new Font("Book Antiqua", Font.BOLD, 20));
		storageLabel.setBounds(125, 525, 170, 35);
		frame.getContentPane().add(storageLabel);
		contentPane.add(storageLabel);
		
		colon1 = new JLabel(":");
		colon1.setHorizontalAlignment(SwingConstants.CENTER);
		colon1.setFont(new Font("Tahoma", Font.BOLD, 20));
		colon1.setBounds(150, 125, 50, 35);
		frame.getContentPane().add(colon1);
		contentPane.add(colon1);
		
		colon2 = new JLabel(":");
		colon2.setHorizontalAlignment(SwingConstants.CENTER);
		colon2.setFont(new Font("Tahoma", Font.BOLD, 20));
		colon2.setBounds(295, 225, 50, 35);
		frame.getContentPane().add(colon2);
		contentPane.add(colon2);
		
		colon3 = new JLabel(":");
		colon3.setHorizontalAlignment(SwingConstants.CENTER);
		colon3.setFont(new Font("Tahoma", Font.BOLD, 20));
		colon3.setBounds(295, 275, 50, 35);
		frame.getContentPane().add(colon3);
		contentPane.add(colon3);
		
		colon4 = new JLabel(":");
		colon4.setHorizontalAlignment(SwingConstants.CENTER);
		colon4.setFont(new Font("Tahoma", Font.BOLD, 20));
		colon4.setBounds(295, 425, 50, 35);
		frame.getContentPane().add(colon4);
		contentPane.add(colon4);
		
		colon5 = new JLabel(":");
		colon5.setHorizontalAlignment(SwingConstants.CENTER);
		colon5.setFont(new Font("Tahoma", Font.BOLD, 20));
		colon5.setBounds(295, 475, 50, 35);
		frame.getContentPane().add(colon5);
		contentPane.add(colon5);
		
		colon6 = new JLabel(":");
		colon6.setHorizontalAlignment(SwingConstants.CENTER);
		colon6.setFont(new Font("Tahoma", Font.BOLD, 20));
		colon6.setBounds(295, 525, 50, 35);
		frame.getContentPane().add(colon6);
		contentPane.add(colon6);
		
		colon7 = new JLabel(":");
		colon7.setHorizontalAlignment(SwingConstants.CENTER);
		colon7.setFont(new Font("Tahoma", Font.BOLD, 20));
		colon7.setBounds(295, 325, 50, 35);
		frame.getContentPane().add(colon7);
		contentPane.add(colon7);
		
		label94 = new JTextField();
		label94.setText("+94");
		label94.setHorizontalAlignment(SwingConstants.CENTER);
		label94.setFont(new Font("Tahoma", Font.PLAIN, 17));
		label94.setEditable(false);
		label94.setColumns(10);
		label94.setBounds(345, 225, 50, 35);
		frame.getContentPane().add(label94);
		contentPane.add(label94);
		
		/*
		 * Customer Information
		 */
		
		name = new JTextField();
		name.setFont(new Font("Tahoma", Font.PLAIN, 20));
		name.setBounds(200, 125, 550, 35);
		frame.getContentPane().add(name);
		name.setColumns(10);
		contentPane.add(name);
		
		emailAddress = new JTextField();
		emailAddress.setFont(new Font("Tahoma", Font.PLAIN, 17));
		emailAddress.setColumns(10);
		emailAddress.setBounds(345, 275, 400, 35);
		frame.getContentPane().add(emailAddress);
		contentPane.add(emailAddress);
		
		format = NumberFormat.getInstance();
		format.setGroupingUsed(false);
		formatter = new NumberFormatter(format);
		formatter.setValueClass(Integer.class);
		formatter.setMinimum(null);
		formatter.setMaximum(Integer.MAX_VALUE);
		formatter.setAllowsInvalid(false);
		formatter.setCommitsOnValidEdit(true);
		
		mobileNum = new JFormattedTextField(formatter);
		mobileNum.setFont(new Font("Tahoma", Font.PLAIN, 17));
		mobileNum.setBounds(395, 225, 200, 35);
		frame.getContentPane().add(mobileNum);
		contentPane.add(mobileNum);
		
		contactMode = new JComboBox<Object>();
		contactMode.setModel(new DefaultComboBoxModel<Object>(new String[] {"SELECT", "E-MAIL", "TEXT", "BOTH"}));
		contactMode.setMaximumRowCount(4);
		contactMode.setFont(new Font("Tahoma", Font.PLAIN, 15));
		contactMode.setBounds(345, 325, 150, 35);
		frame.getContentPane().add(contactMode);
		contentPane.add(contactMode);
		
		String[] brands = {"Select Brand", "Apple", "Samsung", "Huawei", "Xiaomi", "Nokia"};
    		brandCBox = new JComboBox(brands);
    		brandCBox.setMaximumRowCount(6);
    		brandCBox.setBackground(new Color(224, 255, 255));
    		brandCBox.setFont(new Font("Calibri", Font.PLAIN, 20));
    		brandCBox.setBounds(345, 425, 400, 35);
    		frame.getContentPane().add(brandCBox);
    		contentPane.add(brandCBox);
    		brandCBox.addActionListener(this);
    		//  prevent action events from being fired when the up/down arrow keys are used
        	brandCBox.putClientProperty("JComboBox.isTableCellEditor", Boolean.TRUE);
                
        	itemNameCBox = new JComboBox<String>();
        	itemNameCBox.setBackground(new Color(224, 255, 255));
        	itemNameCBox.setFont(new Font("Calibri", Font.PLAIN, 20));
        	itemNameCBox.setBounds(345, 475, 400, 35); 
        	frame.getContentPane().add(itemNameCBox);
        	contentPane.add(itemNameCBox);
        	itemNameCBox.putClientProperty("JComboBox.isTableCellEditor", Boolean.TRUE);
		
        	//String[] storage = {"Select Storage", "32 GB & above", "64 GB & above", "128 GB & above", "256 GB & above", "512 GB"};
        	storageCBox = new JComboBox();
        	storageCBox.setBackground(new Color(224, 255, 255));
        	storageCBox.setFont(new Font("Calibri", Font.PLAIN, 20));
        	storageCBox.setBounds(345, 525, 400, 35);
        	frame.getContentPane().add(storageCBox);
        	contentPane.add(storageCBox);
        
		/*
		 *  Buttons
		 */
		
		// Request details Button
		
		btnRequest = new JButton("Send Request");
		btnRequest.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					String cName = name.getText();
					String em = emailAddress.getText();
					String num = mobileNum.getText();
					String contact = (String)contactMode.getSelectedItem();
					String brand = (String)brandCBox.getSelectedItem();
					String item = (String)itemNameCBox.getSelectedItem();
					String storage = (String)storageCBox.getSelectedItem();
					
					DBConfig connection = new DBConfig();
										
					PreparedStatement ps = connection.getConn().prepareStatement("INSERT INTO pending ("
						    + " name,"
						    + " email,"
						    + " mobile,"
						    + " preferred,"
						    + " brand,"
						    + " item_name,"
						    + " storage ) VALUES ("
						    + "?, ?, ?, ?, ?, ?, ?)");
					ps.setString(1, cName);
					ps.setString(2, em);
					ps.setString(3, num);
					ps.setString(4, contact);
					ps.setString(5, brand);
					ps.setString(6, item);
					ps.setString(7, storage);
					
					int x = ps.executeUpdate();
					if (x == 0) {
						JOptionPane.showMessageDialog(btnRequest, "Something went wrong");
					} else {
						JOptionPane.showMessageDialog(btnRequest, "Request is sucessfully saved");
					}
					connection.getConn().close();
					
					
					if (!cName.isEmpty() && brand!="Select Brand" && item!="Select Item" && storage!="Select Storage") {
						if (contact == "E-MAIL") {
							if (em.isEmpty()) {
								JOptionPane.showMessageDialog(null,  "Please enter your E-mail address to be notified via E-mail ");
							}
							else if (!em.isEmpty()) {	
								JOptionPane.showMessageDialog(null, "Request sent...\n  You will receive an E-mail when this item is available.\nStay Tuned !");
								System.exit(0);
							}
						}
						else if (contact == "TEXT") {
							if (num.isEmpty()) {
								JOptionPane.showMessageDialog(null,  "Please enter your Mobile number to be notified via Text");
							}
							else if (!num.isEmpty()) {
								JOptionPane.showMessageDialog(null,  "Request sent...\n  You will receive a Text when this item is available.\nStay Tuned !");
								System.exit(0);
							}
						}
						else if (contact == "BOTH") {
							if (!em.isEmpty() && !num.isEmpty()) {
								JOptionPane.showMessageDialog(null,  "Request sent...\n  You will receive an E-mail and a Text when this item is available.\nStay Tuned !");
								System.exit(0);
							}
							else {
								JOptionPane.showMessageDialog(null,  "Please enter your E-mail address AND Mobile number to be notified via E-mail AND Text");
							}
						}
						else {
							if (em.isEmpty() && num.isEmpty()) {
								JOptionPane.showMessageDialog(null,  "Please enter your E-mail address OR Mobile number to be notified via E-mail OR Text");
							}
							else {
								JOptionPane.showMessageDialog(null,  "Please select your preferred mode of contact");
							}
						}
					}
					else if (!cName.isEmpty() && (!em.isEmpty() || !num.isEmpty()) && contact!="SELECT" && brand=="Select Brand") {
						JOptionPane.showMessageDialog(null,  "Please select the mobile BRAND");
					}
					else if (!cName.isEmpty() && (!em.isEmpty() || !num.isEmpty()) && contact!="SELECT" && brand!="Select Brand" && item=="Select Item") {
						JOptionPane.showMessageDialog(null,  "Please select the specific ITEM of the selected brand");
					}
					else if (!cName.isEmpty() && (!em.isEmpty() || !num.isEmpty()) && contact!="SELECT" && brand!="Select Brand" && item!="Select Item" && storage=="Select Storage") {
						JOptionPane.showMessageDialog(null,  "Please select the preferred STORAGE OPTION");
					}
					else if (cName.isEmpty()) {
						JOptionPane.showMessageDialog(null,  "Please enter your NAME");
					}
					else {
						JOptionPane.showMessageDialog(null,  "Please enter your details accurately");
					}
					
				}
				catch (Exception ex) {
						JOptionPane.showMessageDialog(null,  "Please enter your details accurately");
					}
			}
			
		});
		btnRequest.setFont(new Font("Tahoma", Font.BOLD, 18));
		btnRequest.setBackground(UIManager.getColor("Button.shadow"));
		btnRequest.setBounds(310, 600, 180, 50);
		frame.getContentPane().add(btnRequest);
		contentPane.add(btnRequest);
		
		// Clear Button
		
		btnClear = new JButton("Clear");
		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				name.setText(null);
				emailAddress.setText(null);
				mobileNum.setValue(null);
				brandCBox.setSelectedIndex(0);
				storageCBox.setSelectedIndex(0);
			}
		});
		btnClear.setFont(new Font("Tahoma", Font.BOLD, 18));
		btnClear.setBackground(UIManager.getColor("Button.light"));
		btnClear.setBounds(175, 675, 150, 30);
		frame.getContentPane().add(btnClear);
		contentPane.add(btnClear);

		// Cancel Button
		
		btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
		           CustomHome frame = new CustomHome();
		           frame.setVisible(true);  
		            dispose();
			}
		});
		btnCancel.setFont(new Font("Tahoma", Font.BOLD, 18));
		btnCancel.setBackground(UIManager.getColor("Button.light"));
		btnCancel.setBounds(475, 675, 150, 30);
		frame.getContentPane().add(btnCancel);
		contentPane.add(btnCancel);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		
		try {
			
			DBConfig connection = new DBConfig();
			String brand = (String)brandCBox.getSelectedItem();
			String query = "select DISTINCT pname,brand from phone where brand = '" + brand + "'";
			Statement sta = connection.getConn().createStatement();
			ResultSet rs = sta.executeQuery(query);
			
			
			while(rs.next()){
				itemNameCBox.addItem(rs.getString("pname"));
			}
			connection.getConn().close();
				
			}catch (Exception e1) {
				e1.printStackTrace();
			}
		try {
			DBConfig connection = new DBConfig();
			String brand = (String)brandCBox.getSelectedItem();
			String query = "select DISTINCT ram,brand from phone where brand = '" + brand + "'";
			Statement sta = connection.getConn().createStatement();
			ResultSet rs = sta.executeQuery(query);
			
			
			while(rs.next()){
				storageCBox.addItem(rs.getString("ram"));
			}
			connection.getConn().close();
				
		}catch (Exception e1) {
			e1.printStackTrace();
		}
	}
}
