 // S14245  Subscription Form

//package forms;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.text.NumberFormatter;

import java.awt.Font;
import java.awt.Toolkit;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JTextField;
import javax.swing.JFormattedTextField;
import com.toedter.calendar.JDateChooser;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ActionListener;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.awt.event.ActionEvent;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

public class SubForm extends JFrame implements ActionListener{

	private JFrame frame;
	private JTextField name;
	private JTextField email;
	private JTextField mob94;
	private JLabel heading;
	private JLabel subHeading;
	private JLabel nameLabel;
	private JLabel emailLabel;
	private JLabel mobileLabel;
	private JLabel dobLabel;
	private JLabel prefLabel;
	private JLabel colon1;
	private JLabel colon2;
	private JLabel colon3;
	private JLabel colon4;
	private JLabel colon5;
	private NumberFormat format;
	private NumberFormatter formatter;
	private JFormattedTextField mobileNum;
	private JDateChooser dateOfBirth;
	private JLabel bdayOffer;
	private JComboBox<Object> contactMode;
	private JButton btnSignUp;
	private JButton btnCancel;
	private JButton btnClear;
	
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	

	/**
	 * Create the application.
	 */
	public SubForm() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setTitle("Subscription Form");
		frame.getContentPane().setBackground(new Color(135, 206, 235));
		frame.setFont(new Font("Arial Narrow", Font.BOLD, 40));
		frame.setBounds(100, 100, 800, 575);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.getContentPane().setLayout(null);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 575);
		Dimension size = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation(size.width / 2 - getWidth() / 2, size.height / 2 - getHeight() / 2);
		setResizable(false);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		getContentPane().setBackground(new Color (135, 206, 235));
		contentPane.setLayout(null);

		/*
		 *  Labels
		 */
		
		heading = new JLabel("SUBSCRIBE TO STAY UPDATED");
		heading.setFont(new Font("Footlight MT Light", Font.BOLD, 35));
		heading.setHorizontalAlignment(SwingConstants.CENTER);
		heading.setBounds(125, 25, 550, 50);
		frame.getContentPane().add(heading);
		contentPane.add(heading);
		
		subHeading = new JLabel("Exclusives, Offers, the Latest and Many More !");
		subHeading.setFont(new Font("Baskerville Old Face", Font.BOLD, 18));
		subHeading.setHorizontalAlignment(SwingConstants.CENTER);
		subHeading.setBounds(180, 65, 450, 50);
		frame.getContentPane().add(subHeading);
		contentPane.add(subHeading);
		
		nameLabel = new JLabel("Name");
		nameLabel.setFont(new Font("Tahoma", Font.BOLD, 20));
		nameLabel.setBounds(75, 125, 175, 25);
		frame.getContentPane().add(nameLabel);
		contentPane.add(nameLabel);
		
		emailLabel = new JLabel("E-mail Address");
		emailLabel.setFont(new Font("Tahoma", Font.BOLD, 20));
		emailLabel.setBounds(75, 175, 175, 25);
		frame.getContentPane().add(emailLabel);
		contentPane.add(emailLabel);
		
		mobileLabel = new JLabel("Mobile Number");
		mobileLabel.setFont(new Font("Tahoma", Font.BOLD, 20));
		mobileLabel.setBounds(75, 225, 175, 25);
		frame.getContentPane().add(mobileLabel);
		contentPane.add(mobileLabel);
		
		mob94 = new JTextField();
		mob94.setEditable(false);
		mob94.setHorizontalAlignment(SwingConstants.CENTER);
		mob94.setFont(new Font("Tahoma", Font.PLAIN, 17));
		mob94.setText("+94");
		mob94.setBounds(300, 225, 50, 25);
		frame.getContentPane().add(mob94);
		contentPane.add(mob94);
		mob94.setColumns(10);
		
		dobLabel = new JLabel("Date of Birth");
		dobLabel.setFont(new Font("Tahoma", Font.BOLD, 20));
		dobLabel.setBounds(75, 275, 175, 25);
		frame.getContentPane().add(dobLabel);
		contentPane.add(dobLabel);
		
		prefLabel = new JLabel("Preferred mode of contact");
		prefLabel.setFont(new Font("Tahoma", Font.BOLD, 20));
		prefLabel.setBounds(75, 325, 275, 25);
		frame.getContentPane().add(prefLabel);
		contentPane.add(prefLabel);
		
		colon1 = new JLabel(":");
		colon1.setHorizontalAlignment(SwingConstants.CENTER);
		colon1.setFont(new Font("Tahoma", Font.BOLD, 20));
		colon1.setBounds(250, 125, 50, 25);
		frame.getContentPane().add(colon1);
		contentPane.add(colon1);
		
		colon2 = new JLabel(":");
		colon2.setHorizontalAlignment(SwingConstants.CENTER);
		colon2.setFont(new Font("Tahoma", Font.BOLD, 20));
		colon2.setBounds(250, 175, 50, 25);
		frame.getContentPane().add(colon2);
		contentPane.add(colon2);
		
		colon3 = new JLabel(":");
		colon3.setHorizontalAlignment(SwingConstants.CENTER);
		colon3.setFont(new Font("Tahoma", Font.BOLD, 20));
		colon3.setBounds(250, 225, 50, 25);
		frame.getContentPane().add(colon3);
		contentPane.add(colon3);
		
		colon4 = new JLabel(":");
		colon4.setHorizontalAlignment(SwingConstants.CENTER);
		colon4.setFont(new Font("Tahoma", Font.BOLD, 20));
		colon4.setBounds(250, 275, 50, 25);
		frame.getContentPane().add(colon4);
		contentPane.add(colon4);
		
		colon5 = new JLabel(":");
		colon5.setHorizontalAlignment(SwingConstants.CENTER);
		colon5.setFont(new Font("Tahoma", Font.BOLD, 20));
		colon5.setBounds(350, 325, 50, 25);
		frame.getContentPane().add(colon5);
		contentPane.add(colon5);
		
		bdayOffer = new JLabel("for birthday offers");
		bdayOffer.setFont(new Font("Tahoma", Font.ITALIC, 15));
		bdayOffer.setBounds(550, 275, 175, 25);
		frame.getContentPane().add(bdayOffer);
		contentPane.add(bdayOffer);
		
		/*
		 *  Customer Information
		 */
		
		name = new JTextField();
		name.setFont(new Font("Tahoma", Font.PLAIN, 17));
		name.setBounds(300, 125, 425, 25);
		frame.getContentPane().add(name);
		contentPane.add(name);
		name.setColumns(10);
		
		email = new JTextField();
		email.setFont(new Font("Tahoma", Font.PLAIN, 17));
		email.setColumns(10);
		email.setBounds(300, 175, 425, 25);
		frame.getContentPane().add(email);
		contentPane.add(email);
		
		format = NumberFormat.getInstance();
		format.setGroupingUsed(false);
		formatter = new NumberFormatter(format);
		formatter.setValueClass(Integer.class);
		formatter.setMinimum(null);
		formatter.setMaximum(Integer.MAX_VALUE);
		formatter.setAllowsInvalid(false);
		formatter.setCommitsOnValidEdit(true);
		
		mobileNum = new JFormattedTextField(formatter); 
		mobileNum.setFont(new Font("Tahoma", Font.PLAIN, 17));
		mobileNum.setBounds(350, 225, 200, 25);
		frame.getContentPane().add(mobileNum);
		contentPane.add(mobileNum);
		
		dateOfBirth = new JDateChooser();
		
		dateOfBirth.setBounds(300, 275, 250, 25);
		frame.getContentPane().add(dateOfBirth);
		contentPane.add(dateOfBirth);
		
		contactMode = new JComboBox<Object>();
		contactMode.setFont(new Font("Tahoma", Font.PLAIN, 15));
		contactMode.setModel(new DefaultComboBoxModel<Object>(new String[] {"SELECT", "E-MAIL", "TEXT", "BOTH"}));
		contactMode.setMaximumRowCount(4);
		contactMode.setBounds(400, 325, 150, 25);
		frame.getContentPane().add(contactMode);
		contentPane.add(contactMode);
		//  prevent action events from being fired when the up/down arrow keys are used
        contactMode.putClientProperty("JComboBox.isTableCellEditor", Boolean.TRUE);
		
		/*
		 * Buttons
		 */ 
		
		// Sign up for subscription Button
		
		btnSignUp = new JButton("Sign Up");
		btnSignUp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					String cName = name.getText();
					String em = email.getText();

					String num = mobileNum.getText();
					String contact = (String)contactMode.getSelectedItem(); 
					SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
					String dob = dateFormat.format(dateOfBirth.getDate());
					
					DBConfig connection = new DBConfig();

					PreparedStatement ps = connection.getConn().prepareStatement("INSERT INTO subscription ("
						    + " name,"
						    + " email," 
						    + " number,"
						    + " contact,"
						    + " dob ) VALUES ("
						    + "?, ?, ?, ?, ?)");
					ps.setString(1, cName);
					ps.setString(2, em);
					ps.setString(3, num);
					ps.setString(4, contact);
					ps.setString(5, dob);
					
					int x = ps.executeUpdate();
					if (x == 0) {
						JOptionPane.showMessageDialog(btnSignUp, "Something went wrong");
					} else {
						JOptionPane.showMessageDialog(btnSignUp, "Subscription is sucessfully saved");
					}
					connection.getConn().close();

					if (cName.isEmpty()) {
						JOptionPane.showMessageDialog(null,  "Please enter your name and contact details");
					}
					else {						
						if (contact == "E-MAIL") {
							if (em.isEmpty()) {
								JOptionPane.showMessageDialog(null,  "Please enter your E-mail address to get E-mail updates");
							}
							else if (!em.isEmpty()) {
								JOptionPane.showMessageDialog(null, "Thank you for subscribing! You will receive Email updates about our EXCLUSIVES, OFFERS, THE LATEST AND MANY MORE!\nStay Tuned !");
								System.exit(0);
							}
						}
						else if (contact == "TEXT") {
							if (num.isEmpty()) {
								JOptionPane.showMessageDialog(null,  "Please enter your Mobile number to get Text updates");
							}
							else if (!num.isEmpty()) {
								JOptionPane.showMessageDialog(null,  "Thank you for subscribing! You will receive Text updates about our EXCLUSIVES, OFFERS, THE LATEST AND MANY MORE!\nStay Tuned !");
								System.exit(0);
							}
						}
						else if (contact == "BOTH") {
							if (!em.isEmpty() && !num.isEmpty()) {
								JOptionPane.showMessageDialog(null,  "Thank you for subscribing! You will receive E-mail and Text updates about our EXCLUSIVES, OFFERS, THE LATEST AND MANY MORE!\nStay Tuned !");
								System.exit(0);
							}
							else {
								JOptionPane.showMessageDialog(null,  "Please enter your E-mail address AND Mobile number to get both E-mail AND Text updates");
							}
						}
						else {
							if (em.isEmpty() && num.isEmpty()) {
								JOptionPane.showMessageDialog(null,  "Please enter your E-mail address OR Mobile number to get E-mail OR Text updates");
							}
							else {
								JOptionPane.showMessageDialog(null,  "Please select your preferred mode of contact");
							}
						}
					}
				}
				catch (Exception ex) {
					JOptionPane.showMessageDialog(null,  "Please enter your details accurately");
				}
				
			}
		});
		btnSignUp.setBackground(UIManager.getColor("Button.shadow"));
		btnSignUp.setFont(new Font("Tahoma", Font.BOLD, 18));
		btnSignUp.setBounds(325, 375, 150, 50);
		frame.getContentPane().add(btnSignUp);
		contentPane.add(btnSignUp);
		
		
		// Cancel Button
		
		btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CustomHome frame = new CustomHome();
	            frame.setVisible(true); 
	            dispose();
			}
		});
		btnCancel.setFont(new Font("Tahoma", Font.BOLD, 18));
		btnCancel.setBackground(UIManager.getColor("Button.light"));
		btnCancel.setBounds(450, 450, 150, 30);
		frame.getContentPane().add(btnCancel);
		contentPane.add(btnCancel);
		
		
		// Clear Button
		
		btnClear = new JButton("Clear");
		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				name.setText(null);
				email.setText(null);
				mobileNum.setValue(null);
				contactMode.setSelectedIndex(0);
				dateOfBirth.setCalendar(null);
			}
		});
		btnClear.setFont(new Font("Tahoma", Font.BOLD, 18));
		btnClear.setBackground(UIManager.getColor("Button.light"));
		btnClear.setBounds(200, 450, 150, 30);
		frame.getContentPane().add(btnClear);
		contentPane.add(btnClear);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}
}
